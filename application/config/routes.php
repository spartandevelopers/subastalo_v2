<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route = [
	'default_controller' => 'HomeController',
	'404_override' => '',
	'translate_uri_dashes' => FALSE,


	'login' => 'HomeController/login',
	'register' => 'HomeController/register',


	'auth' => 'UsersController/auth',
	'register-user' => 'UsersController/register',
	'logout' => 'UsersController/logout',


	'panel' => 'HomeController/panel',
	'panel/product/publish/(:num)' => 'HomeController/product_publish/$1',
	'panel/product/edit/(:num)' => 'HomeController/product_edit/$1',



	'get-category-drop-parent' => 'HomeController/get_category_drop_parent',
	'save-media' => 'HomeController/save_media',
	'get-media' => 'HomeController/get_media',
	'delete-media' => 'HomeController/delete_media',



	'admin' => 'AdminController',
	'admin/products/status/(:num)/(:num)' => 'ProductsController/aprobe/$1/$2',

	'products/add-to-cart/(:num)' => 'ProductsController/add_to_cart/$1'

];


