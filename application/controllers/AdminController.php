<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminController extends CI_Controller {

	public function index()
	{

		$products = $this->product->get_all(false);

		$this->load->view('admin/index',[
			'products' => $products
		]);
	}

}

/* End of file AdminController.php */
/* Location: ./application/controllers/AdminController.php */