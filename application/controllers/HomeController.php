<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeController extends CI_Controller {

	public function index()
	{

		$products = $this->product->get_all(false,[
			'status' => 'success'
		]);

		$this->load->view('home',[
			'products' => $products
		]);
	}

	function login()
	{
		$this->load->view('auth/login');
	}


	function register()
	{
		$this->load->view('auth/register');
	}

	function panel()
	{

		if ($this->session->role == 'admin') {
			redirect('admin','refresh');
		}

		if (!$this->session->id) {
			redirect('login','refresh');
		}

		$products = $this->product->get_all();
		$this->load->view('auth/panel',[
			'products' => $products
		]);
	}

	function product_edit($id,$step = 1)
	{
		$product = $this->product->get($id,false);

		$this->session->set_userdata([
			'product_publish' => [
				'step' => $step,
				'token' => $product->token,
				'owner' => $product->owner,
				'link-video' => $product->link_video,
				'sub-category' => $product->sub_category,
				'category' => $product->category,
				'title' => $product->title,	
				'price' => $product->price,
				'description' => $product->description,
				'limit_days' => $product->limit_days,
				'id' => $product->id
			]
		]);
		$this->product_publish($step);
		return;
		$this->load->view('panel/product_publish',[
			'step' => $step
		]);

	}

	function product_publish($step){

		if (!$this->session->product_publish and $step == 1) {
			$this->session->set_userdata([
				'product_publish' => [
					'step' => 1,
					'token' => str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789".uniqid())
				]
			]);
		}elseif ($step > 1 and !$this->session->product_publish) {
			redirect('panel/product/publish/1','refresh');
		}elseif ($step == 3 and $this->session->product_publish) {
			
			
			$data = $this->session->product_publish;
			unset($data['step']);
			$data['owner'] = $this->session->id;
			$data['link_video'] = $data['link-video'];
			unset($data['link-video']);

			$data['sub_category'] = $data['sub-category'];
			unset($data['sub-category']);

			$this->product->store($data);
			$this->session->unset_userdata('product_publish');

			redirect('panel','refresh');

			return false;
		}

		if ($this->input->post('form')) {
			
			$step = $this->input->post('step');

			if ($step == 1) {
				$this->form_validation->set_rules('category', 'Categoria', 'trim|required');
				$this->form_validation->set_rules('sub-category', 'SubCategoria', 'trim|required');
			}elseif ($step == 2) {
				$this->form_validation->set_rules('title', 'Titulo', 'trim|required');
				$this->form_validation->set_rules('price', 'Precio', 'trim|required');
				$this->form_validation->set_rules('description', 'Descripcion', 'trim|required');
				$this->form_validation->set_rules('limit_days', 'Dias publicados', 'trim|required');
			}

			if(!$this->form_validation->run()){
				echo json_encode([
					'errors' => $this->form_validation->error_array()
				]);
			}else{

				$data = $this->input->post();
				unset($data['form']);
				$product_publish = array_merge($this->session->product_publish, $data);
				$this->session->product_publish = $product_publish;


				if ($step < 3) {
					$next  = $step+1;
					// $this->session->product_publish['step'] = $next;
					echo json_encode([
						'success' => [
							'title' => 'perfecto',
							'message' => 'Espere'
						],
						'redirect' => [
							'location' => base_url('panel/product/publish/'.$next)
						]
					]);

				}

			}

			

			return false;
		}else{
			// echo "<pre>";
			// print_r ($this->session);
			// echo "</pre>";
		}


		$this->load->view('panel/product_publish',[
			'step' => $step
		]);
	}

	function get_category_drop_parent(){
		$categories =  $this->db->get_where('categories',[
			'parent_id' => $this->input->get('id')
		])->result();

		$selected = '';
		if (!$this->session->product_publish['sub-category']) {
			$selected = 'selected';
		}

		$str = "<option $selected disabled>Selecciona una Subcategoria</option>";

		foreach ($categories as $category) {
			$selected = '';
			if ($category->id == $this->session->product_publish['sub-category']) {
				$selected = 'selected';
			}
			$str .= "<option value='$category->id' $selected>$category->title</option>";
		}

		echo $str;
	}

	function save_media(){
		$config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['encrypt_name']			= true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file')){
        	echo "<pre>";
        	print_r ($this->upload->display_errors());
        	echo "</pre>";
        }
        else{
        	$d = $this->upload->data();

        	$data['file_name'] = $d['file_name'];
        	$data['file_type'] = $d['file_type'];
        	$data['file_size'] = $d['file_size'];
        	$data['file_name'] = $d['file_name'];
        	$data['token'] = $this->session->product_publish['token'];
        	$this->db->insert('media', $data);
        }
	}

	function get_media(){
		$images = $this->db->get_where('media',[
			'token' => $this->session->product_publish['token']
		])->result();

		echo json_encode($images);
	}

	function delete_media(){

		$query = $this->db->get_where('media',[
			'file_name' => $this->input->get('name')
		])->num_rows();

		if ($query) {
			$this->db->delete('media',[
				'file_name' => $this->input->get('name')
			]);

			$delete = true;

		}else{
			$delete = false;
		}

		echo json_encode([
			'title' => 'Perfecto',
			'message' => 'Imagen eliminada',
			'delete' => $delete
		]);
	}

}

/* End of file HomeController.php */
/* Location: ./application/controllers/HomeController.php */