<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProductsController extends CI_Controller {

	public function index()
	{
		
	}

	function aprobe($id,$change){
		$product = $this->product->get($id,false);

		if ($change == 1) {
			$product->status = 'success';
		}elseif ($change == 0) {
			$product->status = 'pending';
		}
		
		$this->product->update($product);

		redirect('/admin','refresh');
	}

	function add_to_cart($id){
		$product = $this->product->get($id,true);
		if (!$product || $product->status == 'pending' ) {
			echo 'Este producto ya no esta disponible';

			return false;
		}

		if (!$this->session->has_userdata('cart')) {
			$this->session->set_userdata( ['cart' => []] );
		}

		$cart = $this->session->cart;
		$cart[$product->id] = [
			'id' => $product->id,
			'title' => $product->title,
			'price' => $product->price,
			'image' => $product->images[0]->file_name
		];
		$this->session->set_userdata( ['cart' => $cart] );

		redirect($this->input->get('return'),'refresh');

	}


}

/* End of file ProductsController.php */
/* Location: ./application/controllers/ProductsController.php */