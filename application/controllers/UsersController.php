<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UsersController extends CI_Controller {

	public function index()
	{
		
	}

	function auth()
	{

		$this->form_validation->set_rules('password', 'Contraseña', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');


		if(!$this->form_validation->run()){
			echo json_encode([
				'errors' => $this->form_validation->error_array()
			]);
		}else{

			$user = $this->user->login([
				'email' => $this->input->post('email'),
				'password' => $this->input->post('password')
			]);

			if (!$user->num_rows()) {
				echo json_encode([
					'error' => [
						'title' => 'Usuario no encontrado',
						'message' => 'Verifique sus credenciales'
					]
				]);
			}else{
				$user = $user->row();

				$this->session->set_userdata([
					'email' => $user->email,
					'fullname' => $user->first_name.' '.$user->last_name,
					'role' => $user->role,
					'id' => $user->id
				]);

				echo json_encode([
					'success' => [
						'title' => 'perfecto',
						'message' => 'Sera redireccionado a su panel de control'
					],
					'redirect' => [
						'location' => base_url('panel')
					]
				]);

			}
		}
	}

	function register()
	{
		$this->form_validation->set_rules('first_name', 'Nombre', 'trim|required');
		$this->form_validation->set_rules('last_name', 'Apellido', 'trim|required');
		$this->form_validation->set_rules('password', 'Contraseña', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');


		if(!$this->form_validation->run()){
			echo json_encode([
				'errors' => $this->form_validation->error_array()
			]);
		}else{
			$user = $this->input->post();

			$query = $this->user->store($user);

			echo json_encode([
				'success' => [
					'title' => 'perfecto',
					'message' => 'Usuario guardado satisfactoriamente'
				],
				'redirect' => [
					'location' => base_url('login')
				]
			]);
		}
	}

	function logout(){
		session_destroy();
		redirect('/','refresh');
	}

}

/* End of file UsersController.php */
/* Location: ./application/controllers/UsersController.php */