<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProductModel extends CI_Model {

	function get_categories()
	{
		$categories = $this->db->get_where('categories',['parent_id' => -1])->result();
		foreach ($categories as $category) {
			$category->childrens = $this->db->get_where('categories',[
				'parent_id' => $category->id
			])->result();
		}

		return $categories;
	}

	function store($product){
		$this->db->replace('products', $product);
	}

	function get($id,$info = false){
		$product = $this->db->get_where('products',[
			'id' => $id
 		])->row();

 		if ($info) {
 			$product->owner_info = $this->user->get($product->owner);
 			$product->images = $this->db->get_where('media',[
				'token' => $product->token
			])->result();
			$product->category = $this->db->get_where('categories',[
				'id' => $product->category
			])->row();
			$product->sub_category = $this->db->get_where('categories',[
				'id' => $product->sub_category
			])->row();
 		}


 		return $product;
	}



	function get_all($session = true,$filter = false){

		if ($session) {
			$products = $this->db->from('products')
				->where('owner',$this->session->id)
				->get()->result();
		}else{
			if (!$filter) {
				$products = $this->db->get('products')->result();
			}else{
				$products = $this->db->get_where('products',$filter)->result();
			}
			
		}

		foreach ($products as $key => $product) {
			$products[$key] = $this->get($product->id,true);
		}

		return $products;
	}

	function update($product){
		$this->db->replace('products',$product);
	}

}

/* End of file ProductModel.php */
/* Location: ./application/models/ProductModel.php */