<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserModel extends CI_Model {

	function login($credentials)
	{
		return $this->db->get_where('users',$credentials);
	}

	function store($user)
	{
		$this->db->insert('users', $user);
	}

	function get($id){
		$user = $this->db->get_where('users',[
			'id' => $id
		])->row();

		return $user;
	}

}

/* End of file UserModel.php */
/* Location: ./application/models/UserModel.php */