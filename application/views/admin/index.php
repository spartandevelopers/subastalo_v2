<?php $this->load->view('admin/partial/header'); ?>	

<div class="row">
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Productos</h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<li><a data-action="reload"></a></li>
            		<li><a data-action="close"></a></li>
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			Por favor verifica los <strong>productos</strong> y aprueba su publicación apra que puedan ser vistos en la pagina principal
		</div>

		<table class="table datatable-basic">
			<thead>
				<tr>
					<th>ID</th>
					<th>Titulo</th>
					<th>Precio</th>
					<th>Propietario</th>
					<th>Categoria</th>
					<th>Estado</th>
					<th class="text-center">Actions</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($products as $product): ?>
					<tr>
						<td><?=$product->id?></td>
						<td><?=$product->title?></td>
						<td>$<?=$product->price?></td>
						<td><?=$product->owner_info->email?></td>
						<td><?=$product->category->title?> - <?=$product->sub_category->title?></td>
						<td>
							<?php 
								if ($product->status == 'pending') {
									$text = 'Pendiente de aprobación';
									$class = 'danger';
									$change = 1;
								}elseif ($product->status == 'success') {
									$text = 'Aprobado';
									$class = 'success';
									$change = 0;
								}
							?>
							<span class="label label-<?=$class?>"><?=$text?></span>
						</td>
						<td class="text-center">
							<ul class="icons-list">
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">
										<i class="icon-menu9"></i>
									</a>

									<ul class="dropdown-menu dropdown-menu-right">
										<li>
											<a href="<?=base_url('admin/products/status/'.$product->id.'/'.$change)?>">
												<i class="icon-file-check"></i>
												<?php if ($change == 1): ?>
													Aprobar producto
												<?php elseif ($change == 0): ?>
													Banear producto
												<?php endif ?>
											</a>
										</li>
									</ul>
								</li>
							</ul>
						</td>
					</tr>
				<?php endforeach ?>
				
			</tbody>
		</table>
	</div>
</div>



<?php $this->load->view('admin/partial/footer'); ?>