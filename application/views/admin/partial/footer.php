<!-- Footer -->
					<div class="footer text-muted">
						&copy; 2015. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
					</div>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->


	<!-- Core JS files -->
	<script type="text/javascript" src="<?=base_url('backend')?>/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?=base_url('backend')?>/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?=base_url('backend')?>/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?=base_url('backend')?>/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<script type="text/javascript" src="<?=base_url('backend')?>/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?=base_url('backend')?>/assets/js/plugins/forms/selects/select2.min.js"></script>


	<script type="text/javascript" src="<?=base_url('backend')?>/assets/js/core/app.js"></script>
	<script type="text/javascript" src="<?=base_url('backend')?>/assets/js/app.js"></script>
	<!-- /theme JS files -->

</body>
</html>
