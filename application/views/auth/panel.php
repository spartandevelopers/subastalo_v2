<?php $this->load->view('partial/header'); ?>

<div id="content">
    <?php $this->load->view('partial/breadcrumb',[
        'links' => [
            [
                'title' => 'Home',
                'link' => base_url(),
                'active' => false
            ],
            [
                'title' => 'Panel de control',
                'link' => '#',
                'active' => true
            ]
        ]
    ]); ?>



    <section class="padding-top-40 padding-bottom-60">
        <div class="container">
            <div class="row">

                <!-- Shop Side Bar -->
                <div class="col-md-3">
                    <div class="shop-side-bar">

                        <!-- Categories -->
                        <h6>Menu</h6>
                        <div class="">
                            <ul>
                                <li><a href="#">Productos publicados</a></li>
                                <li><a href="#">Compras</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <!-- Products -->
                <div class="col-md-9">

                    <!-- Short List -->
                    <div class="short-lst">
                        <h2>Productos publicados</h2>
                    </div>

                    <!-- Items -->
                    <div class="col-list">

                        <?php foreach ($products as $product): ?>
                            <?php $this->load->view('products/product',[
                                'product' => $product
                            ]); ?>
                        <?php endforeach ?>
                        
                        <!-- <ul class="pagination">
                            <li>
                                <a href="#" aria-label="Previous"> <i class="fa fa-angle-left"></i> </a>
                            </li>
                            <li><a class="active" href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li>
                                <a href="#" aria-label="Next"> <i class="fa fa-angle-right"></i> </a>
                            </li>
                        </ul> -->
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>

<?php $this->load->view('partial/footer'); ?>
