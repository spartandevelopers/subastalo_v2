<?php $this->load->view('partial/header'); ?>

<div id="content">
    <?php $this->load->view('partial/breadcrumb',[
        'links' => [
            [
                'title' => 'Home',
                'link' => base_url(),
                'active' => false
            ],
            [
                'title' => 'Registro de usuario',
                'link' => '#',
                'active' => true
            ]
        ]
    ]); ?>

    <!-- Blog -->
    <section class="login-sec padding-top-30 padding-bottom-100">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <!-- Login Your Account -->
                    <h5>Registrate</h5>

                    <!-- FORM -->
                    <form action="register-user" method="post" class="ajax-form">
                        <ul class="row">
                            <li class="col-md-6">
                                <label>Nombre <small class="text-danger error validation" data-id="first_name"></small>
                                    <input type="text" class="form-control" name="first_name" placeholder="">
                                </label>
                            </li>
                            <li class="col-md-6">
                                <label>Apellido <small class="text-danger error validation" data-id="last_name"></small> 
                                    <input type="text" class="form-control" name="last_name" placeholder="">
                                </label>
                            </li>
                            <li class="col-sm-12">
                                <label>Email <small class="text-danger error validation" data-id="email"></small>
                                    <input type="text" class="form-control" name="email" placeholder="">
                                </label>
                            </li>
                            <li class="col-sm-12">
                                <label>Contraseña <small class="text-danger error validation" data-id="password"></small>
                                    <input type="password" class="form-control" name="password" placeholder="">
                                </label>
                            </li>
                            <li class="col-sm-12 text-left">
                                <button type="submit" class="btn-round">Iniciar sesion</button>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>

<?php $this->load->view('partial/footer'); ?>
