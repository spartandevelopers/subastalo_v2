<?php $this->load->view('partial/header'); ?>


<div id="content">
    <?php $this->load->view('partial/breadcrumb',[
        'links' => [
            [
                'title' => 'Home',
                'link' => base_url(),
                'active' => false
            ],
            [
                'title' => 'Panel de control',
                'link' => base_url('panel'),
                'active' => false
            ],
            [
                'title' => 'Publica un producto paso: '.$step,
                'link' => '#',
                'active' => true
            ]
        ]
    ]); ?>

    <div class="ship-process padding-top-30 padding-bottom-30">
        <div class="container">
            <ul class="row">

                <li class="col-sm-3 <?=($step == 1) ? 'current' : '';?>">
                    <div class="media-left"> <i class="flaticon-shopping"></i> </div>
                    <div class="media-body"> <span>Paso 1</span>
                        <h6>Selecciona la Categoria</h6>
                    </div>
                </li>
                <li class="col-sm-3 <?=($step == 2) ? 'current' : '';?>">
                    <div class="media-left"> <i class="flaticon-business"></i> </div>
                    <div class="media-body"> <span>Paso 2</span>
                        <h6>Información del producto</h6>
                    </div>
                </li>
            </ul>
        </div>
    </div>


    <section class="shopping-cart padding-bottom-60">
        <div class="container">
            <form action="panel/product/publish/<?=$step?>" method="post" class="ajax-form">
                <input type="hidden" name="form" value="true">
                <input type="hidden" name="id" value="<?=$this->session->product_publish['id']?>">
                <input type="hidden" name="step" value="<?=$step?>">
                <div class="pay-method">
                    <?php if ($step == 1): ?>
                        <div class="row">
                            <div class="col-xs-6">
                                <select name="category" class="selectpicker select-category">
                                    <option disabled="disabled" selected> Selecciona una categoria</option>
                                    <?php foreach ($this->product->get_categories() as $category): ?>
                                        <option value="<?=$category->id?>" <?=($category->id == $this->session->product_publish['category']) ? 'selected' :'';?>> <?=$category->title?></option>
                                    <?php endforeach ?>
                                </select>
                                <small class="text-danger error validation" data-id="category"></small>

                                <?php if ($this->session->product_publish['category']): ?>
                                    <script>
                                        setTimeout(()=> {
                                            $('.select-category').change();
                                        },1000)
                                    </script>
                                <?php endif ?>
                            </div>
                            <div class="col-xs-6">
                                <select name="sub-category" class="btn category-childrens" style="text-align: left; width: 100%;">
                                    <option disabled selected> Subcategorias</option>
                                </select>
                                <small class="text-danger error validation" data-id="sub-category"></small>
                            </div>
                        </div>
                    <?php elseif ($step == 2): ?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="heading">
                                    <h2>Información del producto</h2>
                                    <hr>
                                </div>
                                <div class="information">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label> Titulo del articulo <small class="text-danger error validation" data-id="title"></small>
                                                <input class="form-control" type="text" name="title" value="<?=$this->session->product_publish['title']?>">
                                            </label>
                                        </div>
                                        <div class="col-sm-6">
                                            <label> Precio <small class="text-danger error validation" data-id="price"></small>
                                                <input class="form-control" type="text" name="price" value="<?=$this->session->product_publish['price']?>">
                                            </label>
                                        </div>
                                        <div class="col-sm-12">
                                            <label> Video de youtube <small class="text-danger error validation" data-id="link-video"></small>
                                                <input class="form-control" type="text" name="link-video" value="<?=$this->session->product_publish['link-video']?>">
                                            </label>
                                        </div>
                                        <div class="col-sm-12">
                                            <label>Descripción del articulo <small class="text-danger error validation" data-id="description"></small>
                                                <textarea class="form-control" name="description" id="description" rows="5" placeholder=""><?=$this->session->product_publish['description']?></textarea>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="transportation product">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <small class="text-danger error validation" data-id="limit_days"></small>
                                            <input type="hidden" name="limit_days" value="<?=$this->session->product_publish['limit_days']?>">
                                        </div>
                                        
                                        <div class="col-md-4">
                                            <div class="charges <?=($this->session->product_publish['limit_days'] == 7) ? 'active' : '';?>" data-value="7">
                                                <h6>Subasta por:</h6>
                                                <br>
                                                <span>7 Dias</span>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="charges <?=($this->session->product_publish['limit_days'] == 10) ? 'active' : '';?>" data-value="10">
                                                <h6>Subasta por:</h6>
                                                <br>
                                                <span>10 Dias</span>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="charges <?=($this->session->product_publish['limit_days'] == 12) ? 'active' : '';?>" data-value="12">
                                                <h6>Subasta por:</h6>
                                                <br>
                                                <span>12 Dias</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="heading">
                                    <h2>Imagenes del producto</h2>
                                    <hr>
                                </div>

                                <div class="dropzone" id="dropzone"></div>
                            </div>
                        </div>
                        
                    <?php endif ?>

                 </div>
               
                <div class="pro-btn">
                    <?php if ($step == 1): ?>
                        <a href="#." class="btn-round btn-light">Cancelar</a>
                    <?php else: ?>
                        <a href="<?=base_url('panel/product/publish')?>/<?=$step - 1?>" class="btn-round">Regresar al paso <?=$step - 1?></a>
                    <?php endif ?>

                    <?php if ($step < 2): ?>
                        <button type="submit" href="<?=base_url('panel/product/publish')?>/<?=$step + 1?>" class="btn-round">Seguir al paso <?=$step + 1?></button>
                    <?php else: ?>
                        <button type="submit" href="<?=base_url('panel/product/publish')?>/<?=$step + 1?>" class="btn-round">Publicar</button>
                    <?php endif ?>
                    
                    
                </div>

            </form>
        </div>
    </section>




</div>

<?php $this->load->view('partial/footer'); ?>