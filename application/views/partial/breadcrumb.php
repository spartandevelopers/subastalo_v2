<div class="linking">
    <div class="container">
        <ol class="breadcrumb">
        	<?php foreach ($links as $link): ?>
        		 <li><a href="<?=$link['link']?>" class="<?=($link['active']) ? 'active' : '' ;?>"><?=$link['title']?></a></li>
        	<?php endforeach ?>
        </ol>
    </div>
</div>