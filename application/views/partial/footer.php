<!-- Footer -->
        <footer>
            <div class="container">

                <!-- Footer Upside Links -->
                <div class="foot-link">
                    <ul>
                        <li><a href="#."> Acerca de </a></li>
                        <li><a href="#."> Servicios </a></li>
                        <li><a href="#."> Terminos y condiciones </a></li>
                        <li><a href="#."> Preguntas frecuentes </a></li>
                        <li><a href="#."> Contactanos</a></li>
                    </ul>
                </div>
                <div class="row">

                    <!-- Contact -->
                    <div class="col-md-4">
                        <h4>contacta a subastalo!</h4>
                        <p>Dirección: 45 Grand Central Terminal New York, NY 1017 United State USA</p>
                        <p>Telefono: (+100) 123 456 7890</p>
                        <p>Email: Support@subastalo.com</p>
                        <div class="social-links"> <a href="#."><i class="fa fa-facebook"></i></a> <a href="#."><i class="fa fa-twitter"></i></a> <a href="#."><i class="fa fa-linkedin"></i></a> <a href="#."><i class="fa fa-pinterest"></i></a> <a href="#."><i class="fa fa-instagram"></i></a> <a href="#."><i class="fa fa-google"></i></a> </div>
                    </div>

                    <!-- Categories -->
                    <div class="col-md-3">
                        <h4>Categorias mas vistas</h4>
                        <ul class="links-footer">
                            <?php foreach ($this->product->get_categories() as $key => $category): ?>
                                <li><a href="#."><?=$category->title?></a></li>
                                <?php if ($key > 5) break; ?>
                            <?php endforeach ?>
                        </ul>
                    </div>

                    <!-- Categories -->
                    <div class="col-md-3">
                        <h4>Nuestros serviciós</h4>
                        <ul class="links-footer">
                            <li><a href="#."> Servicio 1</a></li>
                            <li><a href="#."> Servicio 2</a></li>
                            <li><a href="#."> Servicio 3</a></li>
                            <li><a href="#."> Servicio 4</a></li>
                            <li><a href="#."> Servicio 5</a></li>
                        </ul>
                    </div>

                    <!-- Categories -->
                    <div class="col-md-2">
                        <h4>Información</h4>
                        <ul class="links-footer">
                            <li><a href="#."> Acerca de </a></li>
                            <li><a href="#."> Servicios </a></li>
                            <li><a href="#."> Terminos y condiciones </a></li>
                            <li><a href="#."> Preguntas frecuentes </a></li>
                            <li><a href="#."> Contactanos</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>

        <!-- Rights -->
        <div class="rights">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <p>Copyright © 2017 <a href="#." class="ri-li"> SmartTech </a>HTML5 template. All rights reserved</p>
                    </div>
                    <div class="col-sm-6 text-right"> <img src="<?=base_url('template')?>/images/card-icon.png" alt=""> </div>
                </div>
            </div>
        </div>

        <!-- End Footer -->

        <!-- GO TO TOP  -->
        <a href="#" class="cd-top"><i class="fa fa-angle-up"></i></a>
        <!-- GO TO TOP End -->
    </div>
    <!-- End Page Wrapper -->

    <!-- JavaScripts -->
    <script src="<?=base_url('template')?>/js/vendors/jquery/jquery.min.js"></script>
    <script src="<?=base_url('template')?>/js/vendors/wow.min.js"></script>
    <script src="<?=base_url('template')?>/js/vendors/bootstrap.min.js"></script>
    <script src="<?=base_url('template')?>/js/vendors/own-menu.js"></script>
    <script src="<?=base_url('template')?>/js/vendors/jquery.sticky.js"></script>
    <script src="<?=base_url('template')?>/js/vendors/owl.carousel.min.js"></script>

    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
    <script type="text/javascript" src="<?=base_url('template')?>/rs-plugin/js/jquery.tp.t.min.js"></script>
    <script type="text/javascript" src="<?=base_url('template')?>/rs-plugin/js/jquery.tp.min.js"></script>
    <script src="<?=base_url('template')?>/js/main.js"></script>


    <script src="<?=base_url('public/node_modules')?>/izitoast/dist/js/iziToast.min.js"></script>
    <script src="<?=base_url('public/node_modules')?>/dropzone/dist/dropzone.js"></script>


    <script src="<?=base_url('public')?>/assets/js/app.js"></script>
</body>

<!-- Mirrored from event-theme.com/themes/html/smarttech/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 20 Apr 2018 14:17:05 GMT -->

</html>