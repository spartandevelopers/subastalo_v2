<!doctype html>
<html class="no-js" lang="en">

<!-- Mirrored from event-theme.com/themes/html/smarttech/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 20 Apr 2018 14:14:29 GMT -->

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta id="uri" data-content="<?=base_url()?>">
    <!-- Document Title -->
    <title>Subastalo</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="<?=base_url('template')?>/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?=base_url('template')?>/images/favicon.ico" type="image/x-icon">

    <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('template')?>/rs-plugin/css/settings.css" media="screen" />

    <!-- StyleSheets -->
    <link rel="stylesheet" href="<?=base_url('template')?>/css/ionicons.min.css">
    <link rel="stylesheet" href="<?=base_url('template')?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url('template')?>/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?=base_url('template')?>/css/main.css">
    <link rel="stylesheet" href="<?=base_url('template')?>/css/style.css">
    <link rel="stylesheet" href="<?=base_url('template')?>/css/responsive.css">

    <link rel="stylesheet" href="<?=base_url('public/node_modules')?>/izitoast/dist/css/iziToast.min.css">
    
    <link rel="stylesheet" href="<?=base_url('public')?>/assets/css/app.css">

    <!-- Fonts Online -->
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">

    <!-- JavaScripts -->
    <script src="<?=base_url('template')?>/js/vendors/modernizr.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>

    <!-- Page Wrapper -->
    <div id="wrap" class="layout-1">

        <!-- Top bar -->
        <div class="top-bar">
            <div class="container">
                <p>Welcome to SmartTech center!</p>
                <div class="right-sec">
                    <ul>
                        <?php if (!$this->session->id): ?>
                            <li><a href="<?=base_url('login')?>">Login</a></li>
                            <li><a href="<?=base_url('register')?>">Registrate</a></li>
                        <?php else: ?>
                            <li><a href="<?=base_url('panel')?>">Panel</a></li>
                            <li><a href="<?=base_url('logout')?>">Cerrar sesion</a></li>
                            <li><a href="<?=base_url('panel/product/publish/1')?>">Publicar un producto </a></li>
                        <?php endif ?>
                       
                        
                        <li><a href="#.">Newsletter </a></li>
                       
                    </ul>
                    <div class="social-top"> <a href="#."><i class="fa fa-facebook"></i></a> <a href="#."><i class="fa fa-twitter"></i></a></div>
                </div>
            </div>
        </div>

        <!-- Header -->
        <header>
            <div class="container">
                <div class="logo">
                    <a href="index-2.html"><img src="<?=base_url('template')?>/images/logo.png" alt=""></a>
                </div>
                <div class="search-cate">
                    <select class="selectpicker">
                        <option> Todas las categorias</option>
                        <?php foreach ($this->product->get_categories() as $category): ?>
                            <option value="<?=$category->id?>"> <?=$category->title?></option>
                        <?php endforeach ?>
                    </select>
                    <input type="search" placeholder="Busca en la tienda">
                    <button class="submit" type="submit"><i class="icon-magnifier"></i></button>
                </div>

                <!-- Cart Part -->
                <ul class="nav navbar-right cart-pop">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <span class="itm-cont"><?=count($this->session->cart)?></span>
                            <i class="flaticon-shopping-bag"></i>
                            <strong>Mi carrito</strong> <br>
                            <span><?=count($this->session->cart)?> item(s)</span>
                        </a>
                        <ul class="dropdown-menu">
                            <?php foreach ($this->session->cart as $item): ?>
                                 <li>
                                    <div class="media-left">
                                        <a href="#." class="thumb"> <img src="<?=base_url('uploads/'.$item['image'])?>" class="img-responsive" alt=""> </a>
                                    </div>
                                    <div class="media-body"> <a href="#." class="tittle"><?=$item['title']?></a></div>
                                </li>
                            <?php endforeach ?>
                           
                            <li class="btn-cart"> <a href="#." class="btn-round">Produtos en puja</a> </li>
                        </ul>
                    </li>
                </ul>
            </div>

            <!-- Nav -->
            <nav class="navbar ownmenu">
                <div class="container">

                   

                    <!-- Categories -->
                    <div class="cate-lst"> <a data-toggle="collapse" class="cate-style" href="#cater"><i class="fa fa-list-ul"></i> Nuestras categorias </a>
                        <div class="cate-bar-in">
                            <div id="cater" class="collapse">
                                <ul>
                                    <?php foreach ($this->product->get_categories() as $category): ?>
                                        <?php if (!$category->childrens): ?>
                                            <li><a href="<?=base_url('category/'.$category->id)?>"> <?=$category->title?></a></li>
                                        <?php else: ?>
                                            <li class="sub-menu"><a href="<?=base_url('category/'.$category->id)?>"> <?=$category->title?></a>
                                                <ul>
                                                    <?php foreach ($category->childrens as $children): ?>
                                                        <li><a href="<?=base_url('category/'.$children->id)?>"> <?=$children->title?></a></li>
                                                    <?php endforeach ?>
                                                </ul>
                                            </li>
                                        <?php endif ?>
                                        
                                    <?php endforeach ?>                                    
                                </ul>
                            </div>
                        </div>
                    </div>

                    <!-- Navbar Header -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav-open-btn" aria-expanded="false"> <span><i class="fa fa-navicon"></i></span> </button>
                    </div>
                    <!-- NAV -->
                    <div class="collapse navbar-collapse" id="nav-open-btn">
                        <ul class="nav">
                            <li class="dropdown megamenu active"> <a href="index-2.html" class="dropdown-toggle" data-toggle="dropdown">Home </a>
                                <div class="dropdown-menu animated-2s fadeInUpHalf">
                                    <div class="mega-inside scrn">
                                        <ul class="home-links">
                                            <li>
                                                <a href="index-2.html"><img class="img-responsive" src="<?=base_url('template')?>/images/home-1.jpg" alt=""> <span>Home Version 1</span></a>
                                            </li>
                                            <li>
                                                <a href="index-3.html"><img class="img-responsive" src="<?=base_url('template')?>/images/home-2.jpg" alt=""> <span>Home Version 2</span></a>
                                            </li>
                                            <li>
                                                <a href="index-4.html"><img class="img-responsive" src="<?=base_url('template')?>/images/home-3.jpg" alt=""> <span>Home Version 3</span></a>
                                            </li>
                                            <li>
                                                <a href="index-5.html"><img class="img-responsive" src="<?=base_url('template')?>/images/home-4.jpg" alt=""> <span>Home Version 4</span></a>
                                            </li>
                                            <li>
                                                <a href="index-6.html"><img class="img-responsive" src="<?=base_url('template')?>/images/home-5.jpg" alt=""> <span>Home Version 5</span></a>
                                            </li>
                                            <li>
                                                <a href="index-7.html"><img class="img-responsive" src="<?=base_url('template')?>/images/home-6.jpg" alt=""> <span>Home Version 6</span></a>
                                            </li>
                                            <li>
                                                <a href="index-8.html"><img class="img-responsive" src="<?=base_url('template')?>/images/home-7.jpg" alt=""> <span>Home Version 7</span></a>
                                            </li>
                                            <li>
                                                <a href="index-9.html"><img class="img-responsive" src="<?=base_url('template')?>/images/home-8.jpg" alt=""> <span>Home Version 8</span></a>
                                            </li>
                                            <li>
                                                <a href="index-10.html"><img class="img-responsive" src="<?=base_url('template')?>/images/home-9.jpg" alt=""> <span>Home Version 9</span></a>
                                            </li>
                                            <li>
                                                <a href="index-11.html"><img class="img-responsive" src="<?=base_url('template')?>/images/home-10.jpg" alt=""> <span>Home Version 10</span></a>
                                            </li>
                                            <li>
                                                <a href="index-12.html"><img class="img-responsive" src="<?=base_url('template')?>/images/home-11.jpg" alt=""> <span>Home Version 11</span></a>
                                            </li>
                                            <li>
                                                <a href="index-13.html"><img class="img-responsive" src="<?=base_url('template')?>/images/home-12.jpg" alt=""> <span>Home Version 12</span></a>
                                            </li>
                                            <li>
                                                <a href="index-14.html"><img class="img-responsive" src="<?=base_url('template')?>/images/home-13.jpg" alt=""> <span>Home Version 13</span></a>
                                            </li>
                                            <li>
                                                <a href="index-15.html"><img class="img-responsive" src="<?=base_url('template')?>/images/home-14.jpg" alt=""> <span>Home Version 14</span></a>
                                            </li>
                                            <li>
                                                <a href="index-16.html"><img class="img-responsive" src="<?=base_url('template')?>/images/home-15.jpg" alt=""> <span>Home Version 15</span></a>
                                            </li>
                                            <li>
                                                <a href="index-17.html"><img class="img-responsive" src="<?=base_url('template')?>/images/home-16.jpg" alt=""> <span>Home Version 16</span></a>
                                            </li>
                                            <li>
                                                <a href="index-18.html"><img class="img-responsive" src="<?=base_url('template')?>/images/home-17.jpg" alt=""> <span>Home Version 17</span></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <li class="dropdown"> <a href="index-2.html" class="dropdown-toggle" data-toggle="dropdown">Pages </a>
                                <ul class="dropdown-menu multi-level animated-2s fadeInUpHalf">
                                    <li><a href="About.html"> About </a></li>
                                    <li><a href="LoginForm.html"> Login Form </a></li>
                                    <li><a href="GridProducts_3Columns.html"> Products 3 Columns </a></li>
                                    <li><a href="GridProducts_4Columns.html"> Products 4 Columns </a></li>
                                    <li><a href="ListProducts.html"> List Products </a></li>
                                    <li><a href="Product-Details.html"> Product Details </a></li>
                                    <li><a href="ShoppingCart.html"> Shopping Cart</a></li>
                                    <li><a href="PaymentMethods.html"> Payment Methods </a></li>
                                    <li><a href="DeliveryMethods.html"> Delivery Methods</a></li>
                                    <li><a href="Confirmation.html"> Confirmation </a></li>
                                    <li><a href="CheckoutSuccessful.html"> Checkout Successful </a></li>
                                    <li><a href="Error404.html"> Error404 </a></li>
                                    <li><a href="contact.html"> Contact </a></li>
                                    <li class="dropdown-submenu"><a href="#."> Dropdown Level </a>
                                        <ul class="dropdown-menu animated-2s fadeInRight">
                                            <li><a href="#.">Level 1</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <!-- Mega Menu Nav -->
                            <li class="dropdown megamenu"> <a href="index-2.html" class="dropdown-toggle" data-toggle="dropdown">Mega menu </a>
                                <div class="dropdown-menu animated-2s fadeInUpHalf">
                                    <div class="mega-inside">
                                        <div class="top-lins">
                                            <ul>
                                                <li><a href="#."> Cell Phones & Accessories </a></li>
                                                <li><a href="#."> Carrier Phones </a></li>
                                                <li><a href="#."> Unlocked Phones </a></li>
                                                <li><a href="#."> Prime Exclusive Phones </a></li>
                                                <li><a href="#."> Accessories </a></li>
                                                <li><a href="#."> Cases </a></li>
                                                <li><a href="#."> Best Sellers </a></li>
                                                <li><a href="#."> Deals </a></li>
                                                <li><a href="#."> All Electronics </a></li>
                                            </ul>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6>Electronics</h6>
                                                <ul>
                                                    <li><a href="#."> Cell Phones & Accessories </a></li>
                                                    <li><a href="#."> Carrier Phones </a></li>
                                                    <li><a href="#."> Unlocked Phones </a></li>
                                                    <li><a href="#."> Prime Exclusive Phones </a></li>
                                                    <li><a href="#."> Accessories </a></li>
                                                    <li><a href="#."> Cases </a></li>
                                                    <li><a href="#."> Best Sellers </a></li>
                                                    <li><a href="#."> Deals </a></li>
                                                    <li><a href="#."> All Electronics </a></li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-3">
                                                <h6>Computers</h6>
                                                <ul>
                                                    <li><a href="#."> Computers & Tablets</a></li>
                                                    <li><a href="#."> Monitors</a></li>
                                                    <li><a href="#."> Laptops & tablets</a></li>
                                                    <li><a href="#."> Networking</a></li>
                                                    <li><a href="#."> Drives & Storage</a></li>
                                                    <li><a href="#."> Computer Parts & Components</a></li>
                                                    <li><a href="#."> Printers & Ink</a></li>
                                                    <li><a href="#."> Office & School Supplies </a></li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-2">
                                                <h6>Home Appliances</h6>
                                                <ul>
                                                    <li><a href="#."> Refrigerators</a></li>
                                                    <li><a href="#."> Wall Ovens</a></li>
                                                    <li><a href="#."> Cooktops & Hoods</a></li>
                                                    <li><a href="#."> Microwaves</a></li>
                                                    <li><a href="#."> Dishwashers</a></li>
                                                    <li><a href="#."> Washers</a></li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-4"> <img class=" nav-img" src="<?=base_url('template')?>/images/navi-img.png" alt=""> </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="dropdown"> <a href="blog.html" class="dropdown-toggle" data-toggle="dropdown">Blog</a>
                                <ul class="dropdown-menu multi-level animated-2s fadeInUpHalf">
                                    <li><a href="Blog-2.html">Blog </a></li>
                                    <li><a href="Blog_details.html">Blog Single </a></li>
                                </ul>
                            </li>
                            <li> <a href="shop.html">Buy theme! </a></li>
                        </ul>
                    </div>

                    <!-- NAV RIGHT -->
                    <div class="nav-right"> <span class="call-mun"><i class="fa fa-phone"></i> <strong>Hotline:</strong> (+100) 123 456 7890</span> </div>
                </div>
            </nav>
        </header>