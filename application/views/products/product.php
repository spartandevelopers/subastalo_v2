<style>
    .product .media-left{
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        /*width: 250px;*/
        margin-right: 10px;
    }
</style>


<div class="product">
    <article class="row">
        <!-- Product img -->
        <div class="col-md-4" style="background-image: url('<?=base_url("uploads/".$product->images[0]->file_name)?>'); height: 250px;">
           <div class="item-img">
                <!-- <img class="img-responsive" src="<?=base_url("uploads/".$product->images[0]->file_name)?>" alt=""> -->
            </div>
        </div>
        <!-- Content -->
        <div class=" col-md-8">
            <div class="row">
                <!-- Content Left -->
                <div class="col-sm-7">
                    <span class="tag"><?=$product->category->title?> / <?=$product->sub_category->title?></span>
                    <a href="<?=base_url('product/'.$product->id)?>" class="tittle text-capitalize"><?=$product->title?></a>
                    <!-- Reviews -->
                    <p class="rev">
                        <!-- <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>  -->
                        <span class="">(0) Personas interesadas</span>
                    </p>
                    <ul class="bullet-round-list">
                        <p><?=substr($product->description,0,80)?></p>
                    </ul>
                </div>
                <!-- Content Right -->
                <div class="col-sm-5 text-center">
                    <?php if ($this->session->id == $product->owner): ?>
                        <a href="<?=base_url('panel/product/edit/'.$product->id)?>" class="heart"><i class="fa fa-pencil"></i></a>
                    <?php endif ?>
                    <!-- <a href="#." class="heart"><i class="fa fa-heart"></i></a>
                    <a href="#." class="heart navi"><i class="fa fa-navicon"></i></a> -->
                    <div class="position-center-center">
                        <div class="price">$<?=$product->price?></div>
                        <?php if ($this->session->id == $product->owner): ?>
                            <?php 
                                if ($product->status == 'pending') {
                                    $class = 'text-danger';
                                }else {
                                    $class = 'in-stock';
                                }
                            ?>
                            <p>Estado: <span class="<?=$class?>">
                                <?php if ($product->status == 'pending'): ?>
                                    No publicado
                                <?php endif ?>
                            </span></p>
                        <?php endif ?>
                        
                        <a href="#." class="btn-round">
                            <i class="icon-basket-loaded"></i> 
                            Agregar al carrito
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </article>
</div>