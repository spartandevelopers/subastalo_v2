let ajxfuntion = res => {
	console.log(res);
	if (res.errors) {
		$.each(Object.keys(res.errors), function (index, val) {
			$(`.validation[data-id="${val}"]`).text(res.errors[val]);
		});
	}

	if (res.error) {
		let error = res.error;
		iziToast.error({
		    title: error.title,
		    message: error.message
		});
	}

	if (res.success) {
		let success = res.success;
		iziToast.success({
		    title: success.title,
		    message: success.message
		});
	}

	if (res.redirect) {
		let redirect = res.redirect;

		setTimeout(() => {
			location.href = redirect.location;
		}, redirect.timeout);
	}
};

$('.ajax-form').on('submit', function (event) {
	event.preventDefault();


	$.ajax({
		url: uri+$(this).attr('action'),
		type: $(this).attr('method'),
		dataType: 'json',
		data: $(this).serializeArray(),
		beforeSend: () => {
			$.each($('.validation'), function (index, val) {
				$(val).text('');
			});
		},
		success: ajxfuntion

	});

	return false;
});

let uri = $('#uri').data('content');


$('.select-category').on('change', function() {
	
	value = $(this).val();
	$.ajax({
		url: uri+'get-category-drop-parent',
		type: 'get',
		dataType: 'html',
		data: {id: value},
		success: (res) => {
			$('.category-childrens').empty().append(res)
		}
	});
});

$('.transportation.product .charges').on('click', function() {
	$('.charges').removeClass('active')
	$(this).addClass('active');

	$('input[name="limit_days"]').val($(this).data('value'));
});

drawImages = () => {

	$.ajax({
		url: uri+'/get-media',
		type: 'get',
		dataType: 'json',
		success: (res) => {
			$.each(res, function(index, val) {
				let mockFile = { 
                    name: val.file_name, 
                    size: val.file_size, 
                    type: val.file_type,
                    status: Dropzone.ADDED, 
                    url: uri+'/uploads/'+val.file_name
                };

                // Call the default addedfile event handler
                myDropzone.options.addedfile.call(myDropzone,mockFile);

                // And optionally show the thumbnail of the file:
                myDropzone.options.thumbnail.call(myDropzone,mockFile, `${uri}uploads/${val.file_name}`);
				myDropzone.options.maxFiles -= 1;

                myDropzone.files.push(mockFile);
			});

			console.log(myDropzone.options.maxFiles)
		}
	});
}

var myDropzone = new Dropzone("div#dropzone", {
	url: uri+"/save-media",
	paramName: 'file',
	maxFiles: 5,
	acceptedFiles: 'image/*',
	addRemoveLinks: true,
	init: (d) => {
		setTimeout(drawImages,1000);		
	}
});

myDropzone.on("removedfile", function(file) {
	$.ajax({
		url: uri+'/delete-media',
		type: 'get',
		dataType: 'json',
		data: {
			name:file.name
		},
		success: (res) => {

			iziToast.info({
			    title: res.title,
			    message: res.message,
			});
			
			if (res.delete) {
				myDropzone.options.maxFiles += 1;
			}

			console.log(myDropzone.options.maxFiles)
		}
	});
});




// $("div#dropzone").dropzone();

